<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class JsonSearch extends Controller
{
 
    public $movieid;

    public function getJsonData($movieid){
        
        $jsonData = file_get_contents("http://www.omdbapi.com/?t='.$movieid'&y=&plot=short&r=json");
        $json = json_decode($jsonData, true);

        if($json['Response'] == 'True'){
            return view('layouts.movieresult')
                ->with([
                    'name' => $json['Title'],
                    'year' => $json['Year']

                ]);
        }
        else{
            return view('layouts.notFound');
        }
    }

    public function searchJsonData(){
        $getData = Input::get('msearch');
        $jsonData = str_replace(' ', '+', $getData);

        $finalJson = file_get_contents("http://www.omdbapi.com/?s=.'$jsonData'&plot=short&r=json");
        $json = json_decode($finalJson, true);
        $counted = count($json['Search']);

        
        return view('layouts.search')
            ->with([
                'allData' => $json,
                'length' => $counted
            ]);
    }

}
