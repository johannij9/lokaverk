<!DOCTYPE_html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>@yield('title')</title>
		<link rel="stylesheet" href="../css/style.css">
		<link rel="stylesheet" href="../css/bootstrap.min.css">
	</head>
	<body>
		<nav class="navbar navbar-default navbar-fixed-top">
      		<div class="container">
        		<div class="navbar-header">
          			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            			<span class="sr-only">Toggle navigation</span>
            			<span class="icon-bar"></span>
            			<span class="icon-bar"></span>
            			<span class="icon-bar"></span>
          			</button>
          			<a class="navbar-brand" href="{{ url('/') }}">MovieB</a>
        		</div>
        		<div id="navbar" class="navbar-collapse collapse">
          			<ul class="nav navbar-nav">
            			<li><a href="{{ url('about') }}">Um síðuna</a></li>
            			<li><a href="{{ url('auth/login') }}">Innskráning</a></li>
            			<li><a href="{{ url('auth/register') }}">Skrá notenda</a></li>
          			</ul>
          			{!! Form::open(array('url' => 'search', 'class' => 'navbar-form navbar-right')) !!}
          				<div class='form-group'>
        					<?php echo Form::text('msearch', '', array('class' => 'form-control', 'placeholder' => 'Leita...')) ?>
        				</div>

        				<?php echo Form::submit('Leita', array('class' => 'btn btn-success')); ?>
        			{!! Form::close() !!}
        		</div><!--/.nav-collapse -->
      		</div>
    	</nav>
		<div id="mainContent">
			@yield('content')
		</div>

		<!-- jQuery and Bootstrap scripts -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="../public/js/bootstrap.min.js"></script>
	</body>
</html>