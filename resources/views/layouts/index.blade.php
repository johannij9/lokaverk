@extends('layouts.mastertemp')

@section('title', 'Heimasíða')

@section('content')
	<div class="jumbotron">
      <div class="container">
        <h1>Velkomin/n</h1>
        <p>
        	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ornare at libero blandit imperdiet. Sed arcu ligula, bibendum semper magna ac, eleifend facilisis dolor. 
        	Donec scelerisque tellus libero, ut dapibus urna iaculis vel. Fusce velit dolor, blandit et tortor ac, viverra vehicula nulla. Pellentesque ullamcorper sapien 
        	a mauris facilisis, ac.
        </p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
      </div>
    </div>
@endsection