@extends('layouts.mastertemp')

@section('title', 'Leit')

@section('content')
	<div class="jumbotron">
      <div class="container">
        <ul>
            @for($i = 0; $i < $length; $i++)
                <li class=""><a href="{{ route('getmovie', ['movieid' => str_replace(' ', '+', $allData['Search'][$i]['Title'])]) }}">{{ $allData['Search'][$i]['Title'] }}</a></li>
            @endfor
        </ul>
      </div>
    </div>
@endsection