@extends('layouts.mastertemp')

@section('title', 'Inskránning')

@section('content')

<div class="jumbotron">
    <div class="container">

        <!-- resources/views/auth/login.blade.php -->

        <form method="POST" action="/auth/login" class="form-horizontal">
            {!! csrf_field() !!}

            <div class="form-group">
                <label for="email_login" class="col-sm-2 control-label">Netfang</label>
                <input type="email" id="email_login" name="email" value="{{ old('email') }}">
            </div>

            <div class="form-group">
                <label for="pass_login" class="col-sm-2 control-label">Lykilorð</label>
                <input type="password" id="pass_login" name="password" id="password">
            </div>

            <div class="col-sm-offset-2 col-sm-10">
                <div class="checkbox">
                    <input type="checkbox" name="remember"> Remember Me
                </div>
            </div>

            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Innskrá</button>
            </div>
        </form>
    </div>

@endsection