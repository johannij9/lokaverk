@extends('layouts.mastertemp')

@section('title', 'Skrá notenda')

@section('content')

<div class="jumbotron">
    <div class="container">

        <!-- resources/views/auth/register.blade.php -->

        <form method="POST" action="/auth/register" class="form-horizontal">
            {!! csrf_field() !!}

            <div class="form-group">
                <label for="nafn" class="col-sm-2 control-label">Nafn</label>
                <input type="text" id="nafn" name="name" value="{{ old('name') }}">
            </div>

            <div class="form-group">
                <label for="email_register" class="col-sm-2 control-label">Netfang</label>
                <input type="email" id="email_register" name="email" value="{{ old('email') }}">
            </div>

            <div class="form-group">
                <label for="pass_register" class="col-sm-2 control-label">Lykilorð</label>
                <input type="password" id="pass_register" name="password">
            </div>

            <div class="form-group">
                <label for="passc_register" class="col-sm-2 control-label">Staðfest lykilorð</label>
                <input type="password" id="passc_register" name="password_confirmation">
            </div>

            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Skrá notenda</button>
            </div>
        </form>
    </div>

@endsection